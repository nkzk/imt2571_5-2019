<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();
        $actorsEl = $this->xpath->query('//Actor/Name');
        $i = 0;
        foreach($actorsEl as $actor) {       // for each actor
             $name = $actorsEl->item($i++)->childNodes->item(0);
             $actorid = $this->xpath->query("//Actor[Name= '$name->nodeValue']/@id")->item(0)->nodeValue;
             $roles = $this->xpath->query("//Movie/Cast/Role[@actor='$actorid']");
             if($roles->length > 0) {
             foreach($roles as $role) {     //for each role the actor has
                  $moviename = $role->parentNode->parentNode->getElementsByTagName("Name")->item(0)->textContent;
                  $movieyear = $role->parentNode->parentNode->getElementsByTagName("Year")->item(0)->textContent;
                  //put it in the 2d associative array
                  $result[$name->nodeValue][] = "As ". $role->getAttribute('name') ." in ". $moviename ." (". $movieyear . ")";
              }
           } else $result[$name->nodeValue][] = "None";
        }
        return $result;

    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {

        $actorids = $this->xpath->query('//Actor/@id');
        foreach($actorids as $id){
             $realactor = $this->xpath->evaluate("//Movie/Cast/Role/@actor = '$id->nodeValue'");
                  if ($realactor == 0) {
                       $node = $this->xpath->query("//Actor[@id='$id->nodeValue']")->item(0);
                       $node->parentNode->removeChild($node);
                  }
        }

    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        //To do:
        // Implement functionality as specified
        // role attributes: name, actor, alias?
        $role = $this->doc->createElement('Role');
        $cast = $this->xpath->query("//Subsidiary[@id='$subsidiaryId']/Movie[Name='$movieName' and Year='$movieYear']/Cast")->item(0);
        $cast->appendChild($role);
         $role->setAttribute('name', $roleName);
         $role->setAttribute('actor', $roleActor);
          if(!empty($roleAlias)) $role->setAttribute('alias', $roleAlias); //if not empty string or null, sets alias



    }
}
?>
